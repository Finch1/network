#include<iostream>
#include<algorithm>
#include<vector>
#include<cmath>
#include<ctime>
#include<cstdlib>
using namespace std;

class node{
public:
	const static int CNT = 10;
	const static float learn_sped=0.1;

	float out_w[CNT];//the weight link to next node
	int update_max;//the time per update
	int update_cur;
	float sum_w;//the sum of weight from nodes linked to
	float cur_w;//=func(sum_w)
	float v;//the limit
	bool simulate;//check if the node is prepare to update

	static void init(node ns[CNT]){
		for(int i=0;i<CNT;++i){
			ns[i].update_max=randn(1,5);
			ns[i].update_cur=0;
			ns[i].sum_w=0;
			ns[i].cur_w=randf();
			ns[i].simulate=(randf()>0.5);
			//ns[i].v=randf();
			ns[i].v=0.6;//[WARN]
			for(int j=0;j<CNT;++j){
				ns[i].out_w[j]=(randf()>0.5?-1:1)*randf();
			}
		}
	}

	static void update(node ns[CNT]){
		for(int i=0;i<CNT;++i){
			if(ns[i].simulate || randf()>0.9){//[WARN] there has a code
				ns[i].simulate=true;
				if((++ns[i].update_cur)==ns[i].update_max){
					for(int j=0;j<CNT;++j){
						ns[j].sum_w+=ns[i].out_w[j]*ns[i].cur_w;
					}
					ns[i].update_cur=0;
					ns[i].simulate=false;
				}else ns[i].update_cur++;
			}
		}

		for(int i=0;i<CNT;++i){
			if(ns[i].simulate==false){
				ns[i].cur_w=func(ns[i].sum_w);
				ns[i].simulate=ns[i].cur_w>ns[i].v;
			}
			ns[i].sum_w=0;
		}
	}

	static float func(float u){
		return 1.0/(1.0+exp(-u));
	}

	static int randn(int l,int r){
		return l+(rand()%(r-l+1));
	}

	static float randf(){
		return rand()/float(RAND_MAX);
	}

	static void output(node ns[CNT]){
		for(int i=0;i<CNT;++i){
			/*ns[i].update_max=randn(1,5);
			ns[i].update_cur=0;
			ns[i].sum_w=0;
			ns[i].cur_w=randf();
			ns[i].simulate=(randf()>0.5);
			ns[i].v=randf();*/
			//for(int j=0;j<CNT;++j){
			//	cout<<ns[i].out_w[j]<<" ";
			//}
			//cout<<endl;

			cout<<ns[i].simulate<<" ";
		}
	}
private:

};
node ns[node::CNT];

int main(){
	srand(time(0));
	node::init(ns);
	for(int i=0;i<100;++i){
		node::output(ns);
		node::update(ns);
		cout<<endl;
	}
	return 0;
}